apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "bifrost.fullname" . }}
  labels:
    {{- include "bifrost.labels" . | nindent 4 }}
spec:
  replicas: {{ .Values.replicaCount }}
  strategy:
    type: {{ .Values.strategyType }}
  selector:
    matchLabels:
      {{- include "bifrost.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      labels:
        {{- include "bifrost.selectorLabels" . | nindent 8 }}
    spec:
    {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
    {{- end }}
      serviceAccountName: {{ include "bifrost.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}

      {{- if .Values.hostNetwork }}
      hostNetwork: true
      dnsPolicy: ClusterFirstWithHostNet
      {{- end }}

      {{- if .Values.priorityClassName }}
      priorityClassName: {{ .Values.priorityClassName }}
      {{- end }}

      initContainers:
      - name: init-external-ip
        image: alpine/k8s:1.18.2
        {{- if .Values.global.gateway.enabled }}
        command: ['/scripts/external-ip.sh', '{{ .Values.hostNetwork }}', '{{ .Values.global.gateway.name }}', '{{ include "bifrost.fullname" . }}-external-ip']
        {{- else }}
        command: ['/scripts/external-ip.sh', '{{ .Values.hostNetwork }}', '{{ include "bifrost.fullname" . }}', '{{ include "bifrost.fullname" . }}-external-ip']
        {{- end }}
        volumeMounts:
        - name: scripts
          mountPath: /scripts
      - name: init-keepalive
        image: busybox
        command:
        - /bin/sh
        - -c
        - |
          sysctl -w net.ipv4.tcp_keepalive_time=120
          sysctl -w net.ipv4.tcp_keepalive_intvl=60
          sysctl -w net.ipv4.tcp_keepalive_probes=3
        securityContext:
          privileged: true
      - name: init-thorchain
        image: busybox:1.28
        command: ['sh', '-c', 'until nc -zv {{ .Values.thorApi }}; do echo waiting for thor-api; sleep 2; done']
      - name: init-binance
        image: appropriate/curl
        command: ['sh', '-c', 'until curl -sL --fail {{ include "bifrost.binanceDaemon" . }}/abci_info > /dev/null; do echo waiting for binance; sleep 2; done']
      {{- if .Values.bitcoinDaemon.enabled }}
      - name: init-bitcoin
        image: appropriate/curl
        command: ['sh', '-c', 'until curl -sL --fail --data-binary ''{"jsonrpc": "1.0", "id": "node-status", "method": "getblockchaininfo", "params": []}'' -H "content-type: text/plain;" http://thorchain:password@{{ include "bifrost.bitcoinDaemon" . }} > /dev/null; do echo waiting for bitcoin; sleep 2; done']
      {{- end }}
      {{- if .Values.ethereumDaemon.enabled }}
      - name: init-ethereum
        image: appropriate/curl
        command: ['sh', '-c', 'until curl -sL --fail {{ include "bifrost.ethereumDaemon" . }} > /dev/null; do echo waiting for ethereum; sleep 2; done']
      {{- end }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: {{ include "bifrost.image" . }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          command: ["/scripts/bifrost.sh"]
          {{- if .Values.debug }}
          args: ["bifrost", "-c", "/etc/bifrost/config.json", "-l", "debug"]
          {{- else }}
          args: ["bifrost", "-c", "/etc/bifrost/config.json"]
          {{- end }}
          volumeMounts:
            - mountPath: /var/data/bifrost
              name: data
              subPath: data
            - mountPath: /root/.thorcli
              name: data
              subPath: thorcli
            - mountPath: /etc/bifrost
              name: data
              subPath: etc
          env:
            - name: EXTERNAL_IP
              valueFrom:
                configMapKeyRef:
                  name: {{ include "bifrost.fullname" . }}-external-ip
                  key: externalIP
            - name: NET
              value: {{ include "bifrost.net" . }}
            - name: CHAIN_API
              value: {{ .Values.thorApi }}
            - name: CHAIN_RPC
              value: {{ include "bifrost.thorDaemon" . }}
            - name: BINANCE_HOST
              value: {{ include "bifrost.binanceDaemon" . }}
            - name: BINANCE_START_BLOCK_HEIGHT
              value: "{{ .Values.binanceStartBlockHeight }}"
            - name: BTC_HOST
              value: {{ include "bifrost.bitcoinDaemon" . }}
            - name: BTC_START_BLOCK_HEIGHT
              value: "{{ .Values.bitcoinStartBlockHeight }}"
            - name: ETH_HOST
              value: {{ include "bifrost.ethereumDaemon" . }}
            - name: ETH_START_BLOCK_HEIGHT
              value: "{{ .Values.ethereumStartBlockHeight }}"
            - name: PEER
              value: {{ default .Values.peer .Values.global.peer }}
            - name: SIGNER_NAME
              value: {{ .Values.signer.name }}
            - name: SIGNER_PASSWD
              {{- if default .Values.signer.passwordSecret .Values.global.passwordSecret }}
              valueFrom:
                secretKeyRef:
                  name: {{ default .Values.signer.passwordSecret .Values.global.passwordSecret }}
                  key: password
              {{- else}}
              value: {{ .Values.signer.password }}
              {{- end }}
            {{- if default .Values.signer.mnemonicSecret .Values.global.mnemonicSecret }}
            - name: SIGNER_SEED_PHRASE
              valueFrom:
                secretKeyRef:
                  name: {{ default .Values.signer.mnemonicSecret .Values.global.mnemonicSecret }}
                  key: mnemonic
            {{- end }}
          ports:
            - name: p2p
              containerPort: {{ .Values.service.port.p2p }}
              protocol: TCP
            - name: http
              containerPort: {{ .Values.service.port.http }}
              protocol: TCP
            - name: prometheus
              containerPort: 9000
              protocol: TCP
          resources:
            {{- toYaml .Values.resources | nindent 12 }}

      volumes:
      - name: data
      {{- if and .Values.persistence.enabled (not .Values.persistence.hostPath) }}
        persistentVolumeClaim:
          claimName: {{ if .Values.persistence.existingClaim }}{{ .Values.persistence.existingClaim }}{{- else }}{{ template "bifrost.fullname" . }}{{- end }}
      {{- else if and .Values.persistence.enabled .Values.persistence.hostPath }}
        hostPath:
          path: {{ .Values.persistence.hostPath }}
          type: DirectoryOrCreate
      {{- else }}
        emptyDir: {}
      {{- end }}
      - name: scripts
        configMap:
          name: {{ include "bifrost.fullname" . }}-scripts
          defaultMode: 0777
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
    {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
    {{- end }}
    {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
    {{- end }}
